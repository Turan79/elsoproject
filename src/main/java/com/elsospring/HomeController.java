package com.elsospring;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
  
  @RequestMapping("/")
  public String stories(Model model) {
    model.addAttribute("pageTitle", "Minden napra egy SFJ stori!"); 
    
    return "stories";
  }

}
